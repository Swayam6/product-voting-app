import React from 'react';
import Product from './Product';
import Seed from '../seed';

/**
 * state is owned by the component.
 * 'this.state' is private to the component, but we can update it.
 * Every react component is rendered as function of its "props" and "state". This rendering is known as Deterministic.
 * 
 * For all state modification after the initial state, React provides components the method this.setState()
 * Always treat 'this.state' object as immutable i.e. you cannot directly change it.
 */

class ProductList extends React.Component {

    state = {
        products: []
    };

    // constructor(props) {
    //     super(props);

    //     this.state = {
    //         products: [],
    //     };

    //     // this.handleProductUpVote = this.handleProductUpVote.bind(this); no need as arrow function already binding
    // }

    componentDidMount() {
        this.setState({ products: Seed.products });
    }

    handleProductUpVote = (productId) => {
        // const products = this.state.products;

        // products.forEach((product) => {
        //     if(product.id === productId) {
        //         product.votes += 1;
        //     }
        // }); this is not correct

        const newProducts = this.state.products.map((product) => {
            if(product.id === productId) {
                return {
                    ...product,
                    votes: product.votes + 1
                    // Object.assign({}, product, {votes: product.votes + 1});
                };
            }
            return product;
        });

        this.setState({products: newProducts});

    }

    render() {
        const products = this.state.products.sort((p1, p2) => (p2.votes - p1.votes));

        /*
        -if there are many componenets to be made of same type 
        -here we are mapping all the prodcuts so we have to make sure we pass special props 'key' which react internally uses for itself to understand that the two components that is products are different
        NOTE: -we differ components through id bt special props key is used by react to differ between components
        -key can be anything
        */
        const productComponents = products.map((product) => (
            <Product 
                key = {"product-" + product.id}
                id={product.id}
                title={product.title}
                description={product.description}
                url={product.url}
                votes={product.votes}
                submitterAvatarUrl={product.submitterAvatarUrl}
                productImageUrl={product.productImageUrl}
                onUpVote={ this.handleProductUpVote }
            />
        ));
        return (

        /**The below tag is called React Fragmet tag
        As we can see below there are two elemenets that need to be returned but we cannot return two elements at a time so we will have to keep them in a <div> but this div will be unnecessary so React gives a fragment tag which comes from React.Fragment tag 
        This stats that the elements in the fragment tag are to be placed to their parent tag itself 
        */
        <>
            <h1 className='ui dividing centered header'>Product List</h1>
            <div className='ui unstackable items container'>
                {productComponents}
            </div>
        </>
  
        );
    }
}

/*
Note: {} inside JSX is known as JavaScript Expression

Product Componenet will get the props as fllows:
{
    "id": 4,
    "title": "Some Titkle",
    "description": "Some cool stuff",
}

the element will be created like below will be like below:
React.createElemet('Product', {id:, title:, }, null)
*/

/**
We have learnt the concept of props here.B the help of props we have made product list dynamic.
Remember the duty of providing props to child component is of parent component.
 */

export default ProductList;