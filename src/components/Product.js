import React from "react";

/*
All the props which is passed by the parent to the child can ve accessed with the special object names as props

While the child can read the props, it can't modify them. A child does not own its props

In React, the parent component owns the props. (ProductList owns the props of Product)
React favors the idea of One-Way-Data-Flow.
Data changes ome from the "top" of the app and are propogated "diwnwards" through its various components.

Any time we define our own custom component meethods, we have to manually bind 'this' to the component ourselves
*/
class Product extends React.Component {

    // constructor(props) {
    //     super(props);

    //     // this.handleUpVote = this.handleUpVote.bind(this);
    // }

    handleUpVote = () => {
        //console.log(this);
        this.props.onUpVote(this.props.id);
    }

    render() {
        return (
            <div className="item">
                <div className="image">
                    <img src={this.props.productImageUrl}
                    alt={this.props.title} 
                    />
                </div>
                <div className="middle aligned content">
                    <div className="header">
                        <a onClick={ this.handleUpVote }>
                            <i className="large caret up icon"/>
                        </a>
                        {this.props.votes}
                    </div>
                    <div className="description">
                        <h3><a>{this.props.title}</a></h3>
                        <p>{this.props.description}</p>
                    </div>
                    <div className="extra">
                        <span>Submitted By:</span>
                        <img
                            className="ui avatar image"
                            src={this.props.submitterAvatarUrl}
                            alt={this.props.title}/>
                    </div>
                </div>
            </div>
        );
    }
}

export default Product;