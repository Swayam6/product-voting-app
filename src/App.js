
/*
 JSX was created to make JavaScript representation of HTML more HTML-like. To understand the difference between JSX and HTML consider the following syntax:

React.createElement(tag, properties/attributes/props, innerHTML);

Actual DOM:
<div class="ui items">
    Hello, friend! I am a basic React Component
</div>

React DOM:
React.createElement('div', {className: 'ui items'}, "Hello, friend! I am a basic React Component");

Equivalent JSX:
<div className='ui items'>
    Hello, friend! I am a basic React Component
</div>

Actual DOM:
<div class='ui items'>
    <p>
        Hello, friend! I am a basic React Component
    </p>
</div>

React DOM:
React.creatElement('div', {className: 'ui items'},
    React.createElement('p', null, "Hello, friend! I am a basic React Component")
    )

Equivalent JSX:
<div className="ui items">
    <p>
        Hello, friend! I am a basic React Component
    </p>
</div>
**/

/*
There are two ways to declare React Components:
1. An ES6 classes
2. Function Components (Functional Programming)

Sample Class Component
class HelloWorld extends React.Component {

  
  render() {
    //render function should return jsx
    return (
      <div>
        <h2>Hello World From React!</h2>
      </div>
    );
  }// end of render
}

Sample Functional Coponent:
function HelloWorld() {
  return (
    <div>
        <h2>Hello World From React!</h2>
      </div>
  );
}

*/

function App() {
  return (
    <div>
      <h2>Hello World From React!</h2>
    </div>
  );
}

export default App;
